const supertest = require('supertest')
const app = require("../utruck");
const req = supertest(app);
const { setupDB } = require('../test-setup')
const { Truck } = require('../src/models/truckModel')
const databaseName = "utruckTrucksTest";

let token, truckId;

setupDB(databaseName);

beforeAll(async () => {
  const register = await req
    .post("/api/auth/register")
    .send({
      email: "testdriver@test.com",
      password: "123456",
      role: "DRIVER"
    })

  const loginRes = await req
    .post("/api/auth/login")
    .send({
      email: "testdriver@test.com",
      password: "123456"
    })

  token = loginRes.body.jwt_token;
});


it('Create Truck', async () => {
  const res = await req
    .post("/api/trucks")
    .set('Authorization', `Bearer ${token}`)
    .send({
      type: "SPRINTER"
    })

  const truck = await Truck.findOne({})
  expect(truck).toBeTruthy();
  expect(truck.type).toBe('SPRINTER');

  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Truck created successfully');
});

it('Create Another Truck', async () => {
  const res = await req
    .post("/api/trucks")
    .set('Authorization', `Bearer ${token}`)
    .send({
      type: "SMALL STRAIGHT"
    })

  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Truck created successfully');
});

it('Get Trucks', async () => {
  const res = await req
    .get("/api/trucks")
    .set('Authorization', `Bearer ${token}`)
  truckId = res.body.trucks[0]._id;  
  expect(res.statusCode).toBe(200)
  expect(res.body.trucks.length === 2).toBeTruthy();
  expect(res.body.trucks[0].status === 'IS').toBeTruthy();
});

it('Get specific truck', async () => {
  const res = await req
    .get(`/api/trucks/${truckId}`)
    .set('Authorization', `Bearer ${token}`)
  expect(res.statusCode).toBe(200)
  expect(res.body.truck.status === 'IS').toBeTruthy();
  expect(res.body.truck.assigned_to).toBe(null);
});

it('Edit Truck', async () => {
  const res = await req
    .put(`/api/trucks/${truckId}`)
    .set('Authorization', `Bearer ${token}`)
    .send({
      type: "LARGE STRAIGHT"
    })

  const truck = await Truck.findOne({_id: truckId});

  expect(truck.type).toBe('LARGE STRAIGHT');
  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Truck details changed successfully');
});

it('Assign Truck', async () => {
  const res = await req
    .post(`/api/trucks/${truckId}/assign`)
    .set('Authorization', `Bearer ${token}`)

  const truck = await Truck.findOne({_id: truckId});

  expect(truck.assigned_to).toEqual(truck.created_by);
  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Truck assigned successfully');
});

it('Delete Truck', async () => {
  const res = await req
    .del(`/api/trucks/${truckId}`)
    .set('Authorization', `Bearer ${token}`)
    

  const truck = await Truck.findOne({_id: truckId});

  expect(truck).toBeFalsy();
  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Truck deleted successfully');
});