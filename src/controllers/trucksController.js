const express = require('express')
const router = express.Router()
const { truckValidator } = require('../middlewares/validationMiddleware')

const {
  addTrucks,
  getTrucks,
  getTruckById,
  editTruckById,
  deleteTruckById,
  assignTruckById,
  unassignTruckById
} = require('../services/trucksService')
const asyncWrapper = require('../utils/apiUtils')
const { InvalidRoleError, InvalidRequestError } = require('../utils/errors')

/**
 * @swagger
 * /api/trucks:
 *  post:
 *    tags: 
 *    - 'Trucks'
 *    description: Create new truck (Only for a driver)
 *    responses:
 *      '200':
 *          description: A successful response
 *    security:
 *    - jwt_token: []
 */

router.get('/', asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user

  if (role !== 'DRIVER') {
    throw new InvalidRoleError('Permission denied');
  }

  const trucks = await getTrucks(created_by);
  res.json({ trucks })
}))

router.post('/', truckValidator, asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user

  if (role !== 'DRIVER') {
    throw new InvalidRoleError('Permission denied');
  }

  await addTrucks(created_by, req.body);
  res.json({ message: 'Truck created successfully' })
}))

router.get('/:id', asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user
  const id = req.params.id; 

  if (role !== 'DRIVER') {
    throw new InvalidRoleError('Permission denied');
  }

  const truck = await getTruckById(id, created_by);

  if(!truck) {
    throw new InvalidRequestError('No truck with such id found');
  }

  res.json({ truck })
}))

router.post('/:id/assign', asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user
  const id = req.params.id;

  if (role !== 'DRIVER') {
    throw new InvalidRoleError('Permission denied');
  }

  const truck = await assignTruckById(id, created_by);
  if(!truck) throw new InvalidRequestError('This or another already assigned to you')

  res.json({ message: "Truck assigned successfully" })
}))

router.post('/:id/unassign', asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user
  const id = req.params.id;

  if (role !== 'DRIVER') {
    throw new InvalidRoleError('Permission denied');
  }

  const truck = await unassignTruckById(id, created_by);
  if(!truck) throw new InvalidRequestError('No truck with such id')

  res.json({ message: "Truck unassigned successfully" })
}))

router.put('/:id', asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user
  const id = req.params.id; 
  const data = req.body;

  if (role !== 'DRIVER') {
    throw new InvalidRoleError('Permission denied');
  }

  await editTruckById(id, created_by, data);
  res.json({ message: "Truck details changed successfully" })
}))

router.delete('/:id', asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user

  const id = req.params.id; 

  if (role !== 'DRIVER') {
    throw new InvalidRoleError('Permission denied');
  }

  await deleteTruckById(id, created_by);
  res.json({ message: "Truck deleted successfully" })
}))

module.exports = router