const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')

const { User } = require('../models/userModel');
const { CredentialsError } = require('../utils/errors');

const register = async ({ email, password, role }) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role
  });

  await user.save()
}

const login = async ({ email, password }) => {
  const user = await User.findOne({ email });


  if (!user) {
    throw new CredentialsError('No user found or password invalid');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new CredentialsError('No user found or password invalid');
  }

  const token = jwt.sign({
    _id: user._id,
    email: user.email,
    role: user.role
  }, 'secret');

  return token;
}

//TODO
const rememberPassword = async ({ email }) => {
  const user = await User.findOne({ email });

  if (!user) {
    throw new CredentialsError('No user found or password invalid');
  }
}

module.exports = {
  register,
  login,
  rememberPassword
}