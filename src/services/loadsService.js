const { Load } = require('../models/loadModel')
const { Truck } = require('../models/truckModel')
const mongoose = require('mongoose')
const { InvalidRequestError } = require('../utils/errors')
const { sprinter, smallStraight, largeStraight } = require('../utils/truckStats')

const getShipperLoads = async (created_by, lim, offset, status) => {
  let load;
  if (status === 0) {
    load = await Load.find({ created_by }, '-__v -logs._id').limit(lim).skip(offset)
  } else {
    load = await Load.find({ created_by, status }, '-__v -logs._id').limit(lim).skip(offset)
  }

  return load;
}

const getDriverLoads = async (created_by, lim, offset, status) => {
  let load;
  if (status === 0) {
    load = await Load.find({ assigned_to: created_by }, '-__v -logs._id').limit(lim).skip(offset)
  } else {
    load = await Load.find({ assigned_to: created_by, status }, '-__v -logs._id').limit(lim).skip(offset)
  }

  return load;
}

const addLoads = async (created_by, loadBody, logs) => {
  const load = new Load({ ...loadBody, created_by, logs });
  await load.save();
}

const getLoad = async (created_by, id) => {
  const load = await Load.find({ _id: id, created_by }, '-__v -logs._id')
  return load;
}

const editLoad = async (created_by, id, data) => {
  await Load.updateOne({ _id: id, created_by }, { $set: data })
}

const deleteLoad = async (created_by, id) => {
  await Load.findOneAndRemove({ _id: id, created_by });
}

const getActiveLoad = async (created_by) => {
  const load = await Load.findOne({ assigned_to: created_by, status: 'ASSIGNED' }, '-__v -logs._id');
  return load;
}

const postLoad = async (id) => {
  const truck = await Truck.findOne({ status: 'IS', assigned_to: { $ne: null } })

  await Load.findOneAndUpdate({ _id: id }, { $set: { status: 'POSTED' } })

  if (!truck) {
    await Load.findOneAndUpdate({ _id: id }, {
      $set: {
        status: 'NEW',
        logs: {
          message: 'There is no driver right now',
          time: Date.now()
        }
      }
    })
    throw new InvalidRequestError('There is no driver right now');
  }

  const load = await Load.findOne({ _id: id, assigned_to: null })
  if (!load) {
    await Load.findOneAndUpdate({ _id: id }, {
      $set: {
        status: 'NEW',
        logs: {
          message: 'This load already posted',
          time: Date.now()
        }
      }
    })
    throw new InvalidRequestError('This load already posted');
  }
  let typeStats;

  if (truck.type === 'SPRINTER')
    typeStats = sprinter;
  else if (truck.type === 'SMALL STRAIGHT')
    typeStats = smallStraight;
  else
    typeStats = largeStraight;

  const { dimensions, payload } = typeStats;

  if (load.payload > payload) {
    await Load.findOneAndUpdate({ _id: id }, {
      $set: {
        status: 'NEW',
        logs: {
          message: 'There is no truck for this weight load right now',
          time: Date.now()
        }
      }
    })
    throw new InvalidRequestError('There is no truck for this weight load right now')
  } else if (load.dimensions.width > dimensions.width ||
    load.dimensions.length > dimensions.length || load.dimensions.height > dimensions.height) {
    await Load.findOneAndUpdate({ _id: id }, {
      $set: {
        status: 'NEW',
      },
      $push: {
        logs: {
          message: 'There is no truck for these dimensions load right now',
          time: Date.now()
        }
      }
    })
    throw new InvalidRequestError('There is no truck for these dimensions right now')
  }

  await Truck.findOneAndUpdate({ status: 'IS', assigned_to: { $ne: null } }, { $set: { status: 'OL' } })
  await Load.findOneAndUpdate({ _id: id }, {
    $set: {
      state: 'En route to Pick Up',
      status: 'ASSIGNED',
      assigned_to: truck.created_by
    },
    $push: {
      logs: {
        message: `Load assigned to driver with id ${truck.created_by}`,
        time: Date.now()
      }
    }
  })
}

const changeActiveLoadState = async (created_by) => {
  let load = await Load.findOne({ assigned_to: created_by, status: { $ne: 'SHIPPED' } })
  if (!load) throw new InvalidRequestError('There is no active loads')

  switch (load.state) {
    case 'En route to Pick Up':
      await Load.findOneAndUpdate({ assigned_to: created_by, status: { $ne: 'SHIPPED' } }, {
        $set: {
          state: 'Arrived to Pick Up'
        },
        $push: {
          logs: {
            message: `Driver arrived to pick up`,
            time: Date.now()
          }
        }
      })
      break;
    case 'Arrived to Pick Up':
      await Load.findOneAndUpdate({ assigned_to: created_by, status: { $ne: 'SHIPPED' } }, {
        $set: {
          state: 'En route to delivery'
        },
        $push: {
          logs: {
            message: `Driver en route to delivery`,
            time: Date.now()
          }
        }
      })
      break;
    case 'En route to delivery':
      await Load.findOneAndUpdate({ assigned_to: created_by, status: { $ne: 'SHIPPED' } }, {
        $set: {
          state: 'Arrived to delivery',
          status: 'SHIPPED'
        },
        $push: {
          logs: {
            message: `Load was shipped successfully by driver`,
            time: Date.now()
          }
        }
      })
      await Truck.findOneAndUpdate({ created_by: load.assigned_to }, { $set: { status: 'IS' } })
      break;
    default:

      break;
  }

  const id = load._id;

  load = await Load.findOne({ _id: id })
  return load.state;
}

const getActiveLoadsForShipper = async (id, created_by) => {
  const load = await Load.findOne({ _id: id, created_by: created_by, status: { $nin: ['NEW', 'SHIPPED'] } }, '-__v -logs._id')
  const truck = await Truck.findOne({created_by: load.assigned_to, status: 'OL'}, '-__v');
  
  return {
    load, truck
  }
}

module.exports = {
  getShipperLoads,
  getDriverLoads,
  addLoads,
  getLoad,
  editLoad,
  deleteLoad,
  getActiveLoad,
  postLoad,
  changeActiveLoadState,
  getActiveLoadsForShipper
}