const jwt = require('jsonwebtoken')
const { CredentialsError } = require('../utils/errors')

const authMiddleware = (req, res, next) => {
  const {
    authorization
  } = req.headers;

  if (!authorization) {
    throw new CredentialsError('Please include auth header');
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    throw new CredentialsError('Please include token to request');
  }

  try {
    const tokenPayLoad = jwt.verify(token, 'secret');
    req.user = {
      created_by: tokenPayLoad._id,
      email: tokenPayLoad.email,
      role: tokenPayLoad.role
    }
    next();
  }
  catch (err) {
    res.status(401).json({ message: err.message });
  }
}

module.exports = authMiddleware