const express = require('express')
const router = express.Router()


const { getUserInfo, editUserPassword, deleteUser  } = require('../services/usersService')
const asyncWrapper = require('../utils/apiUtils')
const { InvalidRequestError } = require ('../utils/errors')

/**
 * @swagger
 * /api/users/me:
 *  get:
 *    tags: 
 *    - 'Users'
 *    description: Get info about user
 *    responses:
 *      '200':
 *          description: A successful response
 *    security:
 *    - jwt_token: []
 */

router.get('/me', asyncWrapper(async (req, res) => {
  const {
    created_by
  } = req.user

  const user = await getUserInfo(created_by);
  
  res.json({ user })
}))

router.delete('/me', asyncWrapper(async (req, res) => {
  const {
    created_by
  } = req.user

  await deleteUser(created_by);
  res.json({ message: "Profile deleted successfully" })
}))

router.patch('/me/password', asyncWrapper(async (req, res) => {
  const {
    created_by
  } = req.user

  const {
    oldPassword,
    newPassword
  } = req.body;

  const oldUser = await getUserInfo(created_by);

  if (!oldUser) {
    throw new InvalidRequestError('There is no profile!');
  }

  const { createdAt, _id, username } = oldUser;

  const user = {};
  user._id = _id;
  user.username = username;
  user.createdAt = createdAt;
  
  await editUserPassword(user._id, oldPassword, newPassword)

  res.json({"message":"Password changed successfully"});
}))

module.exports = router