const supertest = require('supertest')
const app = require("../utruck");
const req = supertest(app);
const { setupDB } = require('../test-setup')
const { Load } = require('../src/models/loadModel')
const databaseName = "utruckLoadsTest";

let tokenShipper, tokenDriver, loadId;

setupDB(databaseName);

beforeAll(async () => {
  const registerShipper = await req
    .post("/api/auth/register")
    .send({
      email: "testshipper@test.com",
      password: "123456",
      role: "SHIPPER"
    })

  const loginShipper = await req
    .post("/api/auth/login")
    .send({
      email: "testshipper@test.com",
      password: "123456"
    })

  const registerDriver = await req
    .post("/api/auth/register")
    .send({
      email: "testdriver@test.com",
      password: "123456",
      role: "DRIVER"
    })

  const loginDriver = await req
    .post("/api/auth/login")
    .send({
      email: "testdriver@test.com",
      password: "123456"
    })

  tokenShipper = loginShipper.body.jwt_token;
  tokenDriver = loginDriver.body.jwt_token;

  await req
    .post("/api/trucks")
    .set('Authorization', `Bearer ${tokenDriver}`)
    .send({
      type: "SPRINTER"
    })


  const res = await req
    .get("/api/trucks")
    .set('Authorization', `Bearer ${tokenDriver}`)
  truckId = res.body.trucks[0]._id;



  await req
    .post(`/api/trucks/${truckId}/assign`)
    .set('Authorization', `Bearer ${tokenDriver}`)
});

it('Create a Load', async () => {
  const res = await req
    .post("/api/loads")
    .set('Authorization', `Bearer ${tokenShipper}`)
    .send({
      "name": "Moving sofa",
      "payload": 100,
      "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
      "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
      "dimensions": {
        "width": 44,
        "length": 32,
        "height": 66
      }
    })

  const load = await Load.findOne({})
  expect(load).toBeTruthy();
  expect(load.payload).toBe(100);
  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Load created successfully');
});

it('Create another Load', async () => {
  const res = await req
    .post("/api/loads")
    .set('Authorization', `Bearer ${tokenShipper}`)
    .send({
      "name": "Moving table",
      "payload": 300,
      "pickup_address": "Flat 43, 10/F, Dimon Building 220 Ivan Road",
      "delivery_address": "Sr. ElHamvro Lacosta Av. Chornapizza N° 3241",
      "dimensions": {
        "width": 47,
        "length": 36,
        "height": 78
      }
    })

  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Load created successfully');
});

it('Get Loads for Shipper', async () => {
  const res = await req
    .get("/api/loads")
    .set('Authorization', `Bearer ${tokenShipper}`)

  loadId = res.body.loads[0]._id;

  expect(res.body.loads.length).toBe(2);
  expect(res.body.loads[0].assigned_to).toBe(null);
  expect(res.body.loads[0].status).toBe('NEW');
  expect(res.statusCode).toBe(200)
});

it('Get Load by id', async () => {
  const res = await req
    .get(`/api/loads/${loadId}`)
    .set('Authorization', `Bearer ${tokenShipper}`)
  expect(res.statusCode).toBe(200)
  expect(res.body.load).toBeTruthy();
});

it('Edit Load by id', async () => {
  const res = await req
    .put(`/api/loads/${loadId}`)
    .set('Authorization', `Bearer ${tokenShipper}`)
    .send({
      "name": "Large Table",
      "payload": 100,
      "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
      "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
      "dimensions": {
        "width": 44,
        "length": 32,
        "height": 66
      }
    })

  const load = await Load.findOne({ _id: loadId });

  expect(load.name).toBe('Large Table');
  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Load details changed successfully')
});

it('Post Load by id', async () => {
  const res = await req
    .post(`/api/loads/${loadId}/post`)
    .set('Authorization', `Bearer ${tokenShipper}`)

  expect(res.body.message).toBe('Load posted successfully')
  expect(res.statusCode).toBe(200)
  expect(res.body.driver_found).toBe(true);
});

it('Get shipping info load by id for Shipper', async () => {
  const res = await req
    .get(`/api/loads/${loadId}/shipping_info`)
    .set('Authorization', `Bearer ${tokenShipper}`)

  expect(res.statusCode).toBe(200)
  expect(res.body.load.status).toBe('ASSIGNED');
  expect(res.body.truck.status).toBe('OL');
});

it('Get active load for Driver', async () => {
  const res = await req
    .get(`/api/loads/active`)
    .set('Authorization', `Bearer ${tokenDriver}`)

  expect(res.statusCode).toBe(200)
  expect(res.body.load.status).toBe('ASSIGNED');
});

it('Driver change load State to Arrived to Pick Up', async () => {
  const res = await req
    .patch(`/api/loads/active/state`)
    .set('Authorization', `Bearer ${tokenDriver}`)

  const load = await Load.findOne({ _id: loadId });


  expect(load.state).toBe('Arrived to Pick Up');
  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Load state changed to \'Arrived to Pick Up\'');
});

it('Driver change load State to En route to delivery', async () => {
  const res = await req
    .patch(`/api/loads/active/state`)
    .set('Authorization', `Bearer ${tokenDriver}`)

  const load = await Load.findOne({ _id: loadId });

  expect(load.state).toBe('En route to delivery');
  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Load state changed to \'En route to delivery\'');
});

it('Driver change load State to Arrived to delivery', async () => {
  const res = await req
    .patch(`/api/loads/active/state`)
    .set('Authorization', `Bearer ${tokenDriver}`)

  const load = await Load.findOne({ _id: loadId });

  expect(load.state).toBe('Arrived to delivery');
  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Load state changed to \'Arrived to delivery\'');
});

it('Delete Load', async () => {
  const res = await req
    .del(`/api/loads/${loadId}`)
    .set('Authorization', `Bearer ${tokenShipper}`)

  const load = await Load.findOne({ _id: loadId });
  
  expect(load).toBeFalsy();
  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Load deleted successfully');
});