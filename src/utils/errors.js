class UtruckError extends Error {
  constructor(message) {
    super(message);
    this.status = 500;
  }
}

class InvalidRoleError extends UtruckError {
  constructor(message) {
    super(message);
    this.status = 403;
  }
}

class InvalidRequestError extends UtruckError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

class CredentialsError extends UtruckError {
  constructor(message) {
    super(message);
    this.status = 401;
  }
}

class InvalidTruckTypeError extends UtruckError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

module.exports = {
  UtruckError,
  InvalidRoleError, 
  InvalidRequestError,
  CredentialsError,
  InvalidTruckTypeError
 }