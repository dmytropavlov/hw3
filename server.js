const app = require('./utruck')
const port = 8080

const mongoose = require('mongoose')
const livereload = require('livereload');
const connectLivereload = require('connect-livereload');

app.use(connectLivereload());

const liveReloadServer = livereload.createServer();
liveReloadServer.watch(__dirname + '/public');

liveReloadServer.server.once("connection", () => {
  setTimeout(() => {
    liveReloadServer.refresh("/");
  }, 100);
});

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://admin:root@cluster0.klsta.mongodb.net/utruck?retryWrites=true&w=majority', {
      useNewUrlParser: true, useUnifiedTopology: true
    });

    app.listen(port, () => console.log('Server started...'));
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}

start();