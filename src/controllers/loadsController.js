const express = require('express')
const router = express.Router()

const asyncWrapper = require('../utils/apiUtils')

const {
  getShipperLoads,
  getDriverLoads,
  addLoads,
  getLoad,
  editLoad,
  deleteLoad,
  getActiveLoad,
  postLoad,
  changeActiveLoadState,
  getActiveLoadsForShipper } = require('../services/loadsService')
const { InvalidRoleError, InvalidRequestError } = require('../utils/errors')

/**
 * @swagger
 * /api/loads:
 *  get:
 *    tags: 
 *    - 'Loads'
 *    description: Register new user
 *    responses:
 *      '200':
 *          description: A successful response
 *    security:
 *    - jwt_token: []
 */

router.get('/', asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user

  const limit = +req.query.limit || 10;
  const offset = +req.query.offset || 0;
  const status = req.query.status || 0;
  let loads;

  if (limit > 50 || limit < 0) throw new InvalidRequestError('Limit cannot be more than 50 and less than 0');

  if (role === 'SHIPPER') loads = await getShipperLoads(created_by, limit, offset, status);

  if (role === 'DRIVER') loads = await getDriverLoads(created_by, limit, offset, status);

  res.json({ loads })
}))


router.get('/active', asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user

  if (role !== 'DRIVER') throw new InvalidRoleError('Permission denied')

  const load = await getActiveLoad(created_by);
  

  res.json({ load })
}))

router.patch('/active/state', asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user

  if (role !== 'DRIVER') throw new InvalidRoleError('Permission denied')
   
   const status = await changeActiveLoadState(created_by);
   
   
  res.json({ message: `Load state changed to '${status}'` })
}))

router.get('/:id', asyncWrapper(async (req, res) => {
  const {
    created_by
  } = req.user

  const { id } = req.params;


  const load = await getLoad(created_by, id);

  if (!load) throw new InvalidRequestError('There is no load with such id')

  res.json({ load })
}))

router.put('/:id', asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user

  if (role !== 'SHIPPER') throw new InvalidRoleError('Permission denied')
  const { id } = req.params;
  const data = req.body;

  await editLoad(created_by, id, data);

  res.json({ message: "Load details changed successfully" })
}))

router.delete('/:id', asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user

  if (role !== 'SHIPPER') throw new InvalidRoleError('Permission denied')
  const { id } = req.params;

  await deleteLoad(created_by, id);

  res.json({ message: "Load deleted successfully" })
}))

router.post('/', asyncWrapper(async (req, res) => {
  const {
    created_by, role
  } = req.user

  if (role !== 'SHIPPER') throw new InvalidRoleError('Permission denied')

  const logs = {
    message: "Load created by shipper",
    time: Date.now()
  }

  await addLoads(created_by, req.body, logs);
  res.json({ message: "Load created successfully" })
}))

router.post('/:id/post', asyncWrapper(async (req, res) => {
  const {
    role
  } = req.user

  if (role !== 'SHIPPER') throw new InvalidRoleError('Permission denied')
  const { id } = req.params;


  await postLoad(id);
  

  res.json({ message: "Load posted successfully", driver_found: true })
}))

router.get('/:id/shipping_info', asyncWrapper(async (req, res) => {
  const {
    role, created_by
  } = req.user
  if (role !== 'SHIPPER') throw new InvalidRoleError('Permission denied')

  const { id } = req.params;

  const { load, truck } = await getActiveLoadsForShipper(id, created_by);
  if(!load) throw new InvalidRequestError('There is no active loads or load\'s id is incorrect')

  res.json({ load, truck })
}))

module.exports = router