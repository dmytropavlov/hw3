const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
  name: {
    type: String,
    required: true
  },
  payload: {
    type: Number,
    required: true
  },
  pickup_address: {
    type: String,
    required: true
  },
  delivery_address: {
    type: String,
    required: true
  },
  dimensions: {
    width: { type: Number, required: true },
    length: { type: Number, required: true },
    height: { type: Number, required: true }
  },
  status: {
    type: String,
    default: "NEW"
  },
  state: {
    type: String,
    default: ""
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true
  },
  created_date: {
    type: Date,
    default: Date.now()
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null
  },
  logs: [{ message: String, time: Date }]
});

module.exports = { Load };