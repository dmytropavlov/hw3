const { Truck } = require('../models/truckModel');
const { InvalidRequestError } = require('../utils/errors');

const getTrucks = async (created_by) => {
  const trucks = await Truck.find({ created_by }, '-__v')
  return trucks;
}

const addTrucks = async (created_by, truckPayLoad) => {
  const truck = new Truck({ ...truckPayLoad, created_by });
  await truck.save();
}

const getTruckById = async (truckId, created_by) => {
  const trucks = await Truck.findOne({ _id: truckId, created_by }, '-__v')
  return trucks;
}

const editTruckById = async (truckId, created_by, data) => {
  await Truck.findOneAndUpdate({ _id: truckId, created_by }, { $set: data })
}

const deleteTruckById = async (truckId, created_by) => {
  await Truck.findOneAndRemove({ _id: truckId, created_by })
}

const assignTruckById = async (truckId, created_by) => {
  const assignedTruck = await Truck.findOne({ created_by: created_by, assigned_to: { $ne: null } })
  if(assignedTruck) throw new InvalidRequestError('You have already assigned truck!')

  const truck = await Truck.findOneAndUpdate({ _id: truckId, assigned_to: null }, { $set: { assigned_to: created_by } })
  return truck
}

const unassignTruckById = async (truckId, created_by) => {

  const truck = await Truck.findOneAndUpdate({ _id: truckId, created_by }, { $set: { assigned_to: null } })
  console.log(truck)
  return truck
}

module.exports = {
  addTrucks,
  getTrucks,
  getTruckById,
  editTruckById,
  deleteTruckById,
  assignTruckById,
  unassignTruckById
}