const sprinter = {
  dimensions: {
    width: 300,
    length: 250,
    height: 170
  },
  payload: 1700
}

const smallStraight = {
  dimensions: {
    width: 500,
    length: 250,
    height: 170
  },
  payload: 2500
}

const largeStraight = {
  dimensions: {
    width: 700,
    length: 350,
    height: 200
  },
  payload: 2500
}

module.exports = {
  sprinter,
  smallStraight,
  largeStraight
}