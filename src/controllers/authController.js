const express = require('express')
const router = express.Router()

const { register, login, rememberPassword } = require('../services/authService')
const asyncWrapper = require('../utils/apiUtils')
const { registrationValidator, forgotPasswordValidator } = require('../middlewares/validationMiddleware')


/**
 * @swagger
 * /api/auth/register:
 *  post:
 *    tags: 
 *    - 'Auth'
 *    description: Register new user
 *    parameters:
 *      - name: email
 *        type: string
 *        in: formData
 *        required: true
 *      - name: password
 *        type: string
 *        in: formData
 *        required: true
 *      - name: role
 *        type: string
 *        in: formData
 *        required: true
 *    responses:
 *      '200':
 *          description: A successful response
 * /api/auth/login:
 *  post:
 *    tags: 
 *    - 'Auth'
 *    description: Login user
 *    parameters:
 *      - name: email
 *        type: string
 *        in: formData
 *        required: true
 *      - name: password
 *        type: string
 *        in: formData
 *        required: true
 *    responses:
 *      '200':
 *          description: A successful response
 * /api/auth/forgot_password:
 *  post:
 *    tags: 
 *    - 'Auth'
 *    description: Send password to email
 *    parameters:
 *      - name: email
 *        type: string
 *        in: formData
 *        required: true
 *    responses:
 *      '200':
 *          description: A successful response
 * securityDefinitions:
 *  jwt_token:
 *    type: "apiKey"
 *    name: "Authorization"
 *    in: "header"
 */

router.post('/register', registrationValidator, asyncWrapper(async (req, res) => {
  const {
    email, password, role
  } = req.body;

  await register({ email, password, role });

  res.json({ message: 'Profile created successfully' });
}))



router.post('/login', asyncWrapper(async (req, res) => {

  const {
    email, password
  } = req.body;

  const jwt_token = await login({ email, password });

  res.json({ jwt_token });
}))

router.post('/forgot_password', forgotPasswordValidator, asyncWrapper(async (req, res) => {

  const {
    email
  } = req.body;

  await rememberPassword({ email });

  res.json({ message: 'New password sent to your email address' });
}))


module.exports = {
  authRouter: router
}