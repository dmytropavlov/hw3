const { InvalidRoleError } = require('../utils/errors')

const roleCheckMiddleware = (req, res, next) => {
  try {
    const { role } = req.user
    
    if (role !== 'DRIVER' && req.originalUrl === 'api/trucks') {
      throw new InvalidRoleError('Permission denied');
    }
    next()
  }
  catch(err) {
    throw new InvalidRoleError('Permission denied')
  }
}

module.exports = roleCheckMiddleware