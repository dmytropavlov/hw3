const supertest = require('supertest')
const app = require("../utruck");
const req = supertest(app);
const { setupDB } = require('../test-setup')
const { User } = require('../src/models/userModel')
const databaseName = "utruckUserInfoTest";

let token;

setupDB(databaseName);

beforeAll(async () => {
  const register = await req
    .post("/api/auth/register")
    .send({
      email: "testdriver@test.com",
      password: "123456",
      role: "DRIVER"
    })

  const loginRes = await req
    .post("/api/auth/login")
    .send({
      email: "testdriver@test.com",
      password: "123456"
    })

  token = loginRes.body.jwt_token;
});


it('Check User Profile', async () => {
  const res = await req
    .get("/api/users/me")
    .set('Authorization', `Bearer ${token}`)

  expect(res.statusCode).toBe(200)
  expect(res.body.user).toBeTruthy();
  expect(res.body.password).toBeFalsy();
});

it('Change User Profile Password', async () => {
  const res = await req
    .patch("/api/users/me/password")
    .set('Authorization', `Bearer ${token}`)
    .send({
      oldPassword: '123456',
      newPassword: '654321'
    })

  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Password changed successfully');
});

it('Login User Profile with changed Password', async () => {
  const res = await req
    .post("/api/auth/login")
    .send({
      email: "testdriver@test.com",
      password: "654321",
    })

  expect(res.body.jwt_token).toBeTruthy();
  expect(res.statusCode).toBe(200)
});

it('Delete User Profile', async () => {
  const res = await req
    .del("/api/users/me")
    .set('Authorization', `Bearer ${token}`)

  //Validation in DB
  const user = await User.findOne({ email: "testdriver@test.com" })

  expect(user).toBeFalsy();
  expect(res.statusCode).toBe(200)
  expect(res.body.message).toBe('Profile deleted successfully');
})