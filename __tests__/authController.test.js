const supertest = require('supertest')
const app = require("../utruck");
const req = supertest(app);
const { setupDB } = require('../test-setup')
const { User } = require('../src/models/userModel')
const databaseName = "utruckAuthTest";

setupDB(databaseName);

it("Driver Registration", async () => {
  const res = await req
    .post("/api/auth/register")
    .send({
      email: "testdriver@test.com",
      password: "123456",
      role: "DRIVER"
    })
    
    //Validation: user saved to DB
    const user = await User.findOne({email: "testdriver@test.com"})

    expect(user).toBeTruthy();
    expect(user.role).toBe('DRIVER');
    expect(res.statusCode).toBe(200)
    expect(res.body.message).toBe('Profile created successfully')
});

it("Driver Login", async () => {
  const res = await req
    .post("/api/auth/login")
    .send({
      email: "testdriver@test.com",
      password: "123456",
    })
    
        
    expect(res.statusCode).toBe(200)
    expect(res.body.jwt_token).toBeTruthy();
});

it("Shipper Registration", async () => {
  const res = await req
    .post("/api/auth/register")
    .send({
      email: "testshipper@test.com",
      password: "123456",
      role: "SHIPPER"
    })
    
    //Validation: user saved to DB
    const user = await User.findOne({email: "testshipper@test.com"})

    expect(user).toBeTruthy();
    expect(user.role).toBe('SHIPPER');
    expect(res.statusCode).toBe(200)
    expect(res.body.message).toBe('Profile created successfully')
});

it("Shipper Login", async () => {
  const res = await req
    .post("/api/auth/login")
    .send({
      email: "testshipper@test.com",
      password: "123456",
    })
    
        
    expect(res.statusCode).toBe(200)
    expect(res.body.jwt_token).toBeTruthy();
});

