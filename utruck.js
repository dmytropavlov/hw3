const morgan = require('morgan');
const express = require('express');
const expressHandlebars = require('express-handlebars')
const cors = require('cors');
const swaggerUI = require('swagger-ui-express')
const swaggerJsDoc = require('swagger-jsdoc')

//swagger docs
const options = {
  swaggerDefinition: {
    info: {
      title: 'uTruck API',
      version: '1.0.0',
      description: 'uTruck API is an interface for application which helps people to deliver different loads'
    },
    servers: [
      {
        url: 'http://localhost:8080'
      }
    ],
  },
  apis: ['./src/controllers/*.js'],
}

const specs = swaggerJsDoc(options)

const app = express();
const port = 8080;

const { authRouter } = require('./src/controllers/authController')
const trucksRouter = require('./src/controllers/trucksController')
const usersRouter = require('./src/controllers/usersController')
const loadsRouter = require('./src/controllers/loadsController')
const authMiddleware = require('./src/middlewares/authMiddleware')
const { UtruckError } = require('./src/utils/errors');
const roleCheckMiddleware = require('./src/middlewares/roleCheckingMiddleware');


app.use(express.json());

app.use(express.urlencoded({
  extended: true
}));

app.use(morgan('tiny'));
app.use(express.static(__dirname + '/public'));

app.engine('handlebars', expressHandlebars({
  defaultLayout: 'main',
  helpers: {
    section: function (name, options) {
      if (!this._sections) this._sections = {};

      this._sections[name] = options.fn(this);
      return null;
    }
  }
}))

app.set('view engine', 'handlebars');

//CLIENT-SIDE

app.get('/', (req, res) => {
  res.redirect(303, '/login');
})

app.get('/login', (req, res) => {
  res.render('login')
})

app.get('/register', (req, res) => {
  res.render('registration')
})

app.use('/api/swagger', swaggerUI.serve, swaggerUI.setup(specs, { customSiteTitle: 'uTruck API' }));

app.use('/api/auth', authRouter);

app.use(authMiddleware);
app.use('/api/trucks', [roleCheckMiddleware], trucksRouter);
app.use('/api/users', usersRouter);
app.use('/api/loads', loadsRouter);

app.use((req, res, next) => {
  res.status(404).json({ message: 'Not Found' })
})

app.use((err, req, res, next) => {
  if (err instanceof UtruckError) {
    return res.status(err.status).json({ message: err.message })
  }
  res.status(500).json({ message: err.message });
})

module.exports = app;