const bcrypt = require('bcrypt')

const { User } = require('../models/userModel');
const { InvalidRequestError } = require('../utils/errors')

const getUserInfo = async (created_by) => {
  const user = await User.findById(created_by, '-__v -password');
  return user;
}

const editUserPassword = async (created_by, password, newPassword) => {
  const user = await User.findOne({_id: created_by});

  if (!user) {
      throw new InvalidRequestError('Invalid email or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
      throw new InvalidRequestError('Invalid email or password');
  }

  let newPass = await bcrypt.hash(newPassword, 10);
  await User.updateOne({_id: created_by}, { $set: { password: newPass } });
}

const deleteUser = async (created_by) => {
  const user = await User.findOneAndRemove({_id: created_by});
  return user;
}

module.exports = { 
  getUserInfo,
  editUserPassword,
  deleteUser
}