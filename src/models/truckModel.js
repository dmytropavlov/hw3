const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
    type: {
        type: String,
        required: true
    },
    status: {
      type: String,
      default: "IS"
    },
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    created_date: {
        type: Date,
        default: Date.now()
    },
    assigned_to: {
      type: mongoose.Schema.Types.ObjectId,
      default: null
    }
});

module.exports = { Truck };