const Joi = require('joi')
const { CredentialsError, InvalidTruckTypeError } = require('../utils/errors')

const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string()
      .min(6)
      .max(20)
      .required(),
    role: Joi.string()
      .pattern(new RegExp('^DRIVER$|^SHIPPER$')).error(new CredentialsError('There is no role like this'))
      .required()
  })

  try {
    await schema.validateAsync(req.body);
    next()
  }
  catch (err) {
    next(new CredentialsError(err.message))
  }
}

const forgotPasswordValidator = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
      .email()
      .required(),
  })

  try {
    await schema.validateAsync(req.body);
    next()
  }
  catch (err) {
    next(new CredentialsError(err.message))
  }
}

const truckValidator = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
      .pattern(new RegExp('^SPRINTER$|^SMALL STRAIGHT$|^LARGE STRAIGHT$'))
        .error(new InvalidTruckTypeError('There is no truck type like this. Try use SPRINTER | SMALL STRAIGHT | LARGE STRAIGHT'))
      .required(),
  })

  try {
    await schema.validateAsync(req.body);
    next()
  }
  catch (err) {
    next(new CredentialsError(err.message))
  }
}

module.exports = {
  registrationValidator, forgotPasswordValidator, truckValidator
}